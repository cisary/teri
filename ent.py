import os, math, sys, time, random
#sys.stdout = open('uploads/entropy/entropy.py.out', 'w+')
#sys.stderr = open('uploads/entropy/entropy.py.err', 'w+')
#import pp

def H(data): 
  if not data: 
    return 0 
  entropy = 0 
  for x in range(256): 
    p_x = float(data.count(chr(x)))/len(data) 
    if p_x > 0: 
      entropy += - p_x*math.log(p_x, 2) 
  return entropy


def entropy_scan (data, block_size) :
  for block in (
    data[x:block_size+x]
    for x in range (len (data) - block_size) ):
    yield H (block)

start_time = time.time()
data = ''.join ( 
  [chr (random.randint (0, 64)) for x in xrange (1024)] +
  [chr (random.randint (0, 255)) for x in xrange (1024)] +
  [chr (random.randint (0, 64)) for x in xrange (1024)] )

#print data
#print list(entropy_scan( data, 256 ))[:1]

#print data
input = open("uploads/entropy/entropy.py.input", "rb")
chunk = []
chunksize=256
block_count=6
i=0
for val in input.read().split():
	if (i == chunksize*block_count):
	   block=[]
	   block_entropy=[]
	   for j in xrange(0,block_count-1):
		print str(j*chunksize)+'->'+ str(((j+1)*chunksize)-1)
#		print len(chunk)
	   	block=''.join ( [chr (chunk[x]) for x in xrange (j*chunksize,((j+1)*chunksize)-1)] )	  
		block_entropy.append(list(entropy_scan(block, chunksize-2 ))[:1])
#		entropy_iterator=list(entropy_scan( block[j], chunksize-1 ))
#		print str(j*chunksize)+'->'+ str(((j+1)*chunksize)-1)
#		print reduce(lambda x, y: x + y, entropy_iterator) / float(len(entropy_iterator))
	   print block_entropy
	   """block1 = ''.join ( [chr (chunk[x]) for x in xrange (0,chunksize-1)] )
	   block2 = ''.join ( [chr (chunk[x]) for x in xrange (chunksize,(chunksize*2)-1)] )
	   block3 = ''.join ( [chr (chunk[x]) for x in xrange (chunksize*2,(chunksize*3)-1)] )
	   block4 = ''.join ( [chr (chunk[x]) for x in xrange (chunksize*3,(chunksize*4)-1)] )
	   block5 = ''.join ( [chr (chunk[x]) for x in xrange (chunksize*4,(chunksize*5)-1)] )
	   block6 = ''.join ( [chr (chunk[x]) for x in xrange (chunksize*5,(chunksize*6)-1)] )
	   blocks=(block1,block2,block3,block4,block5,block6)"""
#	   print "blocks"
#	   print blocks
	   #print chunk
	   #data= ''.join(chunk[:])
	   #data = ''.join ( [chr (chunk[x]) for x in xrange (0,chunksize)] )
	   #data=''.join ( [chr (chunk[x]) for x in xrange (chunksize,(chunksize*2)-1)] )
	   #print "scan:"
	   #entropy_iterator=list(entropy_scan( data, 255 ))
	   #print len(entropy_iterator)
	   #print reduce(lambda x, y: x + y, entropy_iterator) / float(len(entropy_iterator))
	   i=0
	   del chunk[:]
        chunk.append(int(val))
	i+=1
	#print chunk
input.close()


#file = open('uploads/entropy/entropy.py.input', 'rb')
#data = ''.join ( [chr (int(file.read(1))) for x in xrange (50)] )

#print data

"""print "connecting to memcached server "+os.environ["MEMCACHED_IP"]+":"+os.environ["MEMCACHED_PORT"]
mc = memcache.Client([os.environ["MEMCACHED_IP"]+':'+os.environ["MEMCACHED_PORT"]], debug=0)
counter = mc.get("PPSERVER.COUNTER")

ppservers=()
i=0
while i<int(counter):
	ip = str(mc.get("PPSERVER"+str(i)+"IP"))
	port  = str(mc.get("PPSERVER"+str(i)+"PORT"))
	ppservers=ppservers+(ip+":"+port,)
	i+=1

results = []
print "ppserver nodes:"+str(ppservers)

job_server = pp.Server(ppservers=ppservers,secret="kluc")
print "Starting pp with", job_server.get_ncpus(), "workers"

start_time = time.time()




# The following submits 8 jobs and then retrieves the results
#inputs = (100000, 100100, 100200, 100300, 100400, 100500, 100600, 100700)
#jobs = [(input, job_server.submit(sum_primes,(input,), (isprime,), ("math",))) for input in inputs]
#for input, job in jobs:
#    print "Sum of primes below", input, "is", job()
for p in [3,5,6,7]:
    job_server.submit(parallel_function,
            args=(p,),
            depfuncs=(add_one,),
            modules=('os','math'),
            callback=results.append)


job_server.wait()
print results 
"""
print "Time elapsed: ", time.time() - start_time, "s"
#job_server.print_stats()

sys.stdout.flush()
sys.stderr.flush()
