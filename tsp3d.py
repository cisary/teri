import os, math, sys, time
sys.stdout = open('uploads/tsp3d/tsp3d.py.out', 'w+')
sys.stderr = open('uploads/tsp3d/tsp3d.py.err', 'w+')
import memcache
import pp

def isprime(n):
    """Returns True if n is prime and False otherwise"""
    if not isinstance(n, int):
        raise TypeError("argument passed to is_prime is not of 'int' type")
    if n < 2:
        return False
    if n == 2:
        return True
    max = int(math.ceil(math.sqrt(n)))
    i = 2
    while i <= max:
        if n % i == 0:
            return False
        i += 1
    return True

def sum_primes(n):
    """Calculates sum of all primes below given integer n"""
    return sum([x for x in xrange(2,n) if isprime(x)])

print "connecting to memcached server "+os.environ["MEMCACHED_IP"]+":"+os.environ["MEMCACHED_PORT"]
mc = memcache.Client([os.environ["MEMCACHED_IP"]+':'+os.environ["MEMCACHED_PORT"]], debug=0)
counter = mc.get("PPSERVER.COUNTER")

ppservers=()
i=0
while i<int(counter):
	ip = str(mc.get("PPSERVER"+str(i)+"IP"))
	port  = str(mc.get("PPSERVER"+str(i)+"PORT"))
	ppservers=ppservers+(ip+":"+port,)
	i+=1

results = []
print "ppserver nodes:"+str(ppservers)
# To get a list of `ppservers`, skip down to the next section on 
# writing the qsub script
job_server = pp.Server(ppservers=ppservers,secret="kluc")
print "Starting pp with", job_server.get_ncpus(), "workers"

#job1 = job_server.submit(sum_primes, (100,), (isprime,), ("math",))

#result = job1()

#print "Sum of primes below 100 is", result

start_time = time.time()

# The following submits 8 jobs and then retrieves the results
inputs = (100000, 100100, 100200, 100300, 100400, 100500, 100600, 100700)
jobs = [(input, job_server.submit(sum_primes,(input,), (isprime,), ("math",))) for input in inputs]
for input, job in jobs:
    print "Sum of primes below", input, "is", job()

print "Time elapsed: ", time.time() - start_time, "s"
job_server.print_stats()

#for p in [3,5,6,7]:
 #   job_server.submit(parallel_function,
  #          args=(p,),
   #         depfuncs=(add_one,),
    #        modules=('os',),
     #       callback=results.append)


#job_server.wait()
 
print "ok"
sys.stdout.flush()
sys.stderr.flush()
