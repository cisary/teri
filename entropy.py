import os, sys, math, random, string
HOME=os.environ["HOME"]
sys.path.append(HOME+'/python/2.7.2/lib/python2.7/site-packages/')
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from progressBar import *
from binascii import hexlify
from pylab import *
import cPickle

BLOCKSIZE=256
SHOWPROGRESS = 1 


def H(data):
  if not data:
    return 0
  entropy = 0
  for x in range(256):
    p_x = float(data.count(chr(x)))/len(data)
    if p_x > 0:
      entropy += - p_x*math.log(p_x, 2)
  return entropy

def entropy_scan (data, block_size) :
  if SHOWPROGRESS:
      progress = progressBar(0, len(data) - block_size, 77)
  # creates blocks of block_size for all possible offsets ('x'):
  blocks = (data[x : block_size + x] for x in range (len (data) - block_size))
  i = 0
  for block in (blocks) :
    i += 1
    if SHOWPROGRESS:
        progress(i)
    yield H (block)

"""import random
raw = ''.join (
[chr (random.randint (0, 64)) for x in xrange (1024)] +
[chr (random.randint (0, 255)) for x in xrange (1024)] +
[chr (random.randint (0, 64)) for x in xrange (1024)] )
"""

raw = open('uploads/entropy/entropy.py.input', 'rb').read()        
results = list( entropy_scan(raw,BLOCKSIZE) )
print results[:1]
average=reduce(lambda x, y: x + y, results) / float(len(results))
print "Plotting results to output.png"
# Plot
filesize = os.path.getsize('uploads/entropy/entropy.py.input')
imgdpi = 100
imgwidth = filesize / imgdpi

if imgwidth > 327:
      imgwidth = 327

#majorLocator   = MultipleLocator(0x400)   # mark every 1024 bytes
majorLocator   = MultipleLocator(100)
majorFormatter = FormatStrFormatter('%d') # X change to %d to see decimal offsets
left, width = .25, .50
bottom, height = .25, .50
right = left + width
top = bottom + height



ax = subplot(111)
plot(results, linewidth=2.0, antialiased=True)
subplots_adjust(left=0.02, right=0.99, bottom=0.2)

ax.axis([0,filesize,0,8])
ax.xaxis.set_major_locator(majorLocator)
ax.xaxis.set_major_formatter(majorFormatter)
xticks(rotation=315)

xlabel('block (byte offset)')
ylabel('entropy')
title('Entropy levels')
ax.text(0.99, 0.09, 'Average entropy: '+str(average),
        horizontalalignment='right',
        verticalalignment='bottom',
#	backgroundcolor='b',
	weight='bold',
#	color='w',
	alpha=0.8,
	backgroundcolor='w',
	color="b",
        transform=ax.transAxes)
grid(True)

img = gcf()
img.set_size_inches(imgwidth, 6)
img.savefig("uploads/entropy/output.png", dpi=imgdpi)
