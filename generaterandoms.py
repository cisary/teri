from Crypto import Random
import struct
import os
import random
import sys, getopt
sys.stdout = open('uploads/entropy/entropy.py.input', 'w+')

def write_pycrypto_randoms(num):
   for i in xrange(0,num):
	print ord(struct.unpack(">c", Random.get_random_bytes(1))[0])

def write_posix_urandoms(num):
   for i in xrange(0,num):
	print ord(struct.unpack(">c", os.urandom(1))[0])

def write_pseudo_randoms(num,seed):
   random.seed(seed)
   for i in xrange(0,num):
	print random.getrandbits(8)

def main(argv):
   number = 0
   outputfile = ''
   numbers = 0
   try:
      opts, args = getopt.getopt(argv,"hn:m:",["number=","max="])
   except getopt.GetoptError:
      print 'generaterandoms.py -n <number> -m <numbers per file>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'generaterandoms.py -n <number> -m <numbers per file>'
         sys.exit()
      elif opt in ("-n", "--number"):
         number = int(arg)
      elif opt in ("-m", "--max"):
         numbers = int(arg)
   for i in xrange(0,number):
      write_pseudo_randoms(numbers,i)
      write_posix_urandoms(numbers)
      write_pycrypto_randoms(numbers)
   

if __name__ == "__main__":
   main(sys.argv[1:])

sys.stdout.flush()
